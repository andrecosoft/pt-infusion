<?php 

namespace Modules\Notice\Services;

use Exception;
use Modules\CrCore\Exceptions\ResponseException;
use Modules\CrCore\Libs\ServiceResponse\ServiceResponse;
use Modules\Notice\Repositories\NoticeRepository;

class CrudService extends ServiceResponse {

    public $notice_repository;

    public function __construct()
    {
        $this->notice_repository = new NoticeRepository();
    }

    public function saveNotice($title,$content,$autor){

        // -----------------------
        // Aqui se hacen operaciones de validacion para ver que
        // si llega algun campo vacio
        //---------------

        $this->notice_repository->save($title,$content,$autor);
        $this->responseSuccess([],'Noticia guardada con exito');
    }

    public function editNotice($notice_id,$title,$content,$autor){

        // -----------------------
        // Aqui se hacen operaciones de validacion para ver que
        // si llega algun campo vacio
        //---------------

        $this->notice_repository->update($notice_id,$title,$content,$autor);
        $this->responseSuccess([],'Noticia editada con exito');
    }

    public function informationNotice($notice_id){

        // -----------------------
        // Aqui se hacen operaciones de validacion para ver que
        // si llega algun campo vacio
        //---------------


        $notice_information = $this->notice_repository->show($notice_id);
        $this->responseSuccess($notice_information,'Informacion de la noticia');
    }

    public function getAllNotices(){

        // -----------------------
        // Aqui se hacen operaciones de validacion para ver que
        // si llega algun campo vacio
        //---------------


        $list_all_notices = $this->notice_repository->all();
        $this->responseSuccess($list_all_notices,'Listado de noticias');
    }

    public function deleteNotice($notice_id){

        // -----------------------
        // Aqui se hacen operaciones de validacion para ver que
        // si llega algun campo vacio
        //---------------

        $this->notice_repository->delete($notice_id);
        $this->responseSuccess([],'Noticia eliminada');
    }

}

?>