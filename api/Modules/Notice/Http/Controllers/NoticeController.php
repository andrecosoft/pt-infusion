<?php

namespace Modules\Notice\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\CrCore\Exceptions\ResponseException;
use Modules\Notice\Services\CrudService;

class NoticeController extends Controller
{

    public $crud_service;

    public function __construct()
    {
        $this->crud_service = new CrudService();
    }

    public function store(Request $request)
    {
        try{
            $this->crud_service->saveNotice($request->title,$request->content,$request->autor);
            return $this->crud_service->HTTPresponse();
        }catch(ResponseException $e){
            $this->crud_service->responseError401($e->getMessage(),$e->getData());
            return $this->crud_service->HTTPresponse();
        }        
    }

    public function show($notice_id)
    {
        try{
            $this->crud_service->informationNotice($notice_id);
            return $this->crud_service->HTTPresponse();
        }catch(ResponseException $e){
            $this->crud_service->responseError401($e->getMessage(),$e->getData());
            return $this->crud_service->HTTPresponse();
        }
    }

    public function update(Request $request, $notice_id)
    {
        try{
            $this->crud_service->editNotice($notice_id,$request->title,$request->content,$request->autor);
            return $this->crud_service->HTTPresponse();
        }catch(ResponseException $e){
            $this->crud_service->responseError401($e->getMessage(),$e->getData());
            return $this->crud_service->HTTPresponse();
        }
    }

    public function allNotices()
    {
        try{
            $this->crud_service->getAllNotices();
            return $this->crud_service->HTTPresponse();
        }catch(ResponseException $e){
            $this->crud_service->responseError401($e->getMessage(),$e->getData());
            return $this->crud_service->HTTPresponse();
        }
    }

    public function destroy($notice_id)
    {
        try{
            $this->crud_service->deleteNotice($notice_id);
            return $this->crud_service->HTTPresponse();
        }catch(ResponseException $e){
            $this->crud_service->responseError401($e->getMessage(),$e->getData());
            return $this->crud_service->HTTPresponse();
        }
    }
}
