<?php

namespace Modules\Notice\Repositories;

use Modules\Notice\Models\Notice;

class NoticeRepository{
    
    public function save($title,$content,$autor){
        $notice_object             = new Notice();
        $notice_object->title      = $title;
        $notice_object->content    = $content;
        $notice_object->autor      = $autor;
        $notice_object->fechar     = date("Y-m-d H:i:s");
        $notice_object->save();
    }

    public function update($notice_id,$title,$content,$autor){
        $notice_object = Notice::find($notice_id);
        $notice_object->title      = $title;
        $notice_object->content    = $content;
        $notice_object->autor      = $autor;
        $notice_object->update();
    }

    public function show($notice_id){
        return  Notice::find($notice_id)                
                        ->first()
                        ->toArray();
    }

    public function all(){
        return  Notice::all()->toArray() ?? [];
    }

    public function delete($notice_id){
        $notice_object = Notice::find($notice_id);
        $notice_object->delete();
    }

}
