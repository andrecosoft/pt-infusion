<?php

namespace Modules\Login\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\CrCore\Exceptions\ResponseException;
use Modules\Login\Services\LoginService;

class LoginController extends Controller
{

    public $login_service;

    public function __construct()
    {
        $this->login_service = new LoginService();
    }

    public function ingresoUsuario(Request $request)
    {
        try{
            $this->login_service->validateAccesUser($request->email,$request->password);
            return $this->login_service->HTTPresponse();
        }catch(ResponseException $e){
            $this->login_service->responseError401($e->getMessage(),$e->getData());
            return $this->login_service->HTTPresponse();
        }
    }

}
