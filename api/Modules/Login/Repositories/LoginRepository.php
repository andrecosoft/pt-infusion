<?php

namespace Modules\Login\Repositories;

use Modules\User\Models\User;

class LoginRepository{
    
    public function getUserLogin($email,$password){
        return optional(User::where('email',$email)->where('password',md5($password))->first())->toArray() ?? [];
    }
    
}
