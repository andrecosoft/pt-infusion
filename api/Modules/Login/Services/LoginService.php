<?php 

namespace Modules\Login\Services;

use Modules\CrCore\Exceptions\ResponseException;
use Modules\CrCore\Libs\ServiceResponse\ServiceResponse;
use Modules\Login\Repositories\LoginRepository;

class LoginService extends ServiceResponse {

    public $login_repository;

    public function __construct()
    {
        $this->login_repository = new LoginRepository();
    }

    public function validateAccesUser($email,$password){

        // -----------------------
        // Aqui se hacen operaciones de validacion para ver que
        // si llega un email valido y una contraseña.
        //---------------


        $existe_usuario = $this->login_repository->getUserLogin($email,$password);
        if(!empty($existe_usuario)){
            $this->responseSuccess([],'Bienvenido');
        }else{
            throw new ResponseException('El usuario no existe');
        }
    }
}

?>