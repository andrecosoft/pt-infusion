<?php include('header.php'); ?>
<?php include('menu.php'); ?>

<section class="main">
	<div class="bg-light min-vh-100 p-3 p-md-4 p-xl-5 d-flex align-items-center">
		<div class="container">
			<div class="card">
				<div class="card-body">
					<h1 class="mt-0 mb-3">Crear noticia</h1>

					<form name="notice_form" action="#" method="POST">
						<div class="row">
							<div class="col-12 col-sm-6 mb-3">
								<div class="form-group my-0">
									<input type="text" name="title" id="title" placeholder="Título" class="form-control">
								</div>
							</div>
							<div class="col-12 col-sm-6 mb-3">
								<div class="form-group my-0">
									<input type="text" name="autor" id="autor" placeholder="Autor" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<textarea rows="10" name="content" id="content" placeholder="Descripción" class="form-control"></textarea>
						</div>
						<div class="alert alert-success" role="alert" id="msg_ok">
							Publicacion creada con exito
						</div>
						<button type="submit" class="btn btn-primary ml-auto d-table">Publicar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>