<header>
	<nav class="navbar navbar-expand-lg shadow-sm py-3 px-0">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#desplegar" aria-controls="desplegar" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div class="collapse navbar-collapse" id="desplegar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item"><a href="crear-publicacion.php" class="nav-link text-reset px-lg-4">Crear publicación</a></li>
					<li class="nav-item"><a href="listado-noticias.php" class="nav-link text-reset px-lg-4">Listado de publicaciones</a></li>
				</ul>
			</div>
		</div>
	</nav>
</header>
