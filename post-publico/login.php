<?php include('header.php'); ?>

<section class="main">
	<div class="bg-light min-vh-100 p-3 p-md-4 p-xl-5 d-flex align-items-center">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-8 col-md-6 col-lg-5">
					<div class="card">
						<div class="card-body">
							<h1 class="mt-0 text-center mb-3">Login</h1>
							
							<form action="">
								<div class="form-group">
									<input type="text" placeholder="Nombre" class="form-control">
								</div>
								<div class="form-group">
									<input type="text" placeholder="Contraseña" class="form-control">
								</div>
								
								<button type="button" class="btn btn-primary mx-auto d-table">Ingresar</button>
								
								<a href="" class="mt-3 text-center d-block">¿Olvidaste tu contraseña?</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>