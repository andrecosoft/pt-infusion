var SERVICE_URL = 'http://localhost/pt-infusion/api/public/';

var LISTADO_OBJ = {
    Init(){
        this.MODELS.loadNotices();
        this.EVENTS();
    },
    EVENTS:function(){

    },
    MODELS:{
        loadNotices:function(){
            ajax('GET','api/notice/all-notices',null,function(data){
                $.each(data,function(key,information_notice){
                    $('#list_of_notices').append(`
                        <tr>
                            <td>`+ information_notice.title +`</td>
                            <td>`+ information_notice.autor +`</td>
                            <td>`+ information_notice.content +`</td>
                            <td class="text-center">
                                <button class="btn btn-sm btn-success m-1">Editar</button>
                                <button class="btn btn-sm btn-danger m-1">Eliminar</button>
                            </td>
                        </tr>
                    `);
                })
            });
        }
    }
};

var GESTION_OBJ = {
    Init(){
        $('#msg_ok').hide();
        this.EVENTS();
    },
    EVENTS:function(){
        $('[name=notice_form]').on('submit',GESTION_OBJ.MODELS.saveNotice);
    },
    MODELS:{
        saveNotice:function(){
            ajax('POST','api/notice/crud',{
                title: $('#title').val(),
                autor: $('#autor').val(),
                content: $('#content').val()
            },function(data){
                $('#msg_ok').show();
                setTimeout(()=>{
                    location.href = 'listado-noticias.php';
                },3000);
            });

            return false;
        }
    }
};



// CONTROLLER
var current_url = window.location.href;
if(current_url.indexOf('listado-noticias') > -1){
    LISTADO_OBJ.Init();
}else if(current_url.indexOf('crear-publicacion') > -1){
    GESTION_OBJ.Init();
}

// Helpers
function ajax(mode,url,params,cb){
    if(mode == 'GET'){
        $.get(SERVICE_URL + url, function(result, status){
            cb(result.data);
        });
    }else if(mode == 'POST'){
        $.post(SERVICE_URL + url, params,function(result, status){
            cb(result.data);
        });
    }
}