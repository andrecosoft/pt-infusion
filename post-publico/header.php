<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package andreco
 */

?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="profile" href="https://gmpg.org/xfn/11">

<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Lora:wght@400;500;600;700&family=Work+Sans:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css?v=<?= rand(1, 1000); ?>">
<link rel="stylesheet" href="css/all.min.css?v=<?= rand(1, 1000); ?>">
<link rel="stylesheet" href="css/all-coreui.min.css?v=<?= rand(1, 1000); ?>">
<link rel="stylesheet" href="css/startuiicons.min.css?v=<?= rand(1, 1000); ?>">
<link rel="stylesheet" href="css/owl.carousel.min.css?v=<?= rand(1, 1000); ?>">
<link rel="stylesheet" href="css/owl.theme.default.min.css?v=<?= rand(1, 1000); ?>">
<link rel="stylesheet" href="css/main.css?v=<?= rand(1, 1000); ?>">

<script src="js/jquery.min.js?v=<?= rand(1, 1000); ?>"></script>
<script src="js/popper.js?v=<?= rand(1, 1000); ?>"></script>
<script src="js/bootstrap.min.js?v=<?= rand(1, 1000); ?>"></script>
<script src="js/bootstrap.bundle.min.js?v=<?= rand(1, 1000); ?>"></script>
<script src="js/main.js?v=<?= rand(1, 1000); ?>"></script>

</head>

<body>