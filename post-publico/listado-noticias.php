<?php include('header.php'); ?>
<?php include('menu.php'); ?>
<section class="main">
	<div class="bg-light min-vh-100 py-3 py-md-4 py-xl-5 d-flex align-items-center">
		<div class="container">
			<div class="card">
				<div class="card-body">
					<h1 class="mt-0 mb-4">Listado de noticias</h1>
										
					<div class="table-responsive">
						<table width="100%" class="table table-bordered">
							<thead class="text-center">
								<tr>
									<th>Título</th>
									<th>Autor</th>
									<th>Descripción</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody id="list_of_notices">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include('footer.php'); ?>