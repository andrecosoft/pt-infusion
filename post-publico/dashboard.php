<?php include('header.php'); ?>

<header>
	<nav class="navbar navbar-expand-lg shadow-sm py-3 px-0">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#desplegar" aria-controls="desplegar" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div class="collapse navbar-collapse" id="desplegar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item"><a href="#" class="nav-link text-reset px-lg-4">Crear publicación</a></li>
					<li class="nav-item"><a href="#" class="nav-link text-reset px-lg-4">Listado de publicaciones</a></li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<section class="main">
	<div class="bg-light min-vh-100 p-3 p-md-4 p-xl-5 d-flex align-items-center">
		<div class="container">
			<div class="card">
				<div class="card-body">
					<h1 class="mt-0 mb-3">Indicadores</h1>
					
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
							<div class="alert alert-success" role="alert">
								<h5 class="alert-heading">Noticias publicadas</h5>
								<hr>
								<h2 class="my-0 font-weight-bold text-right heading-1 h1">81</h2>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<div class="alert alert-danger" role="alert">
								<h5 class="alert-heading">Noticias vencidas</h5>
								<hr>
								<h2 class="my-0 font-weight-bold text-right heading-1 h1">54</h2>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<div class="alert alert-warning" role="alert">
								<h5 class="alert-heading">Comentarios</h5>
								<hr>
								<h2 class="my-0 font-weight-bold text-right heading-1 h1">110</h2>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<div class="alert alert-info" role="alert">
								<h5 class="alert-heading">Usuarios</h5>
								<hr>
								<h2 class="my-0 font-weight-bold text-right heading-1 h1">32</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row mt-4">
				<div class="col-12 col-lg-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Últimas noticias</h5>
							
							<div class="table-responsive">
								<table class="table table-bordered table-condensed">
									<thead class="text-center">
										<tr>
											<th>Título</th>
											<th>Autor</th>
											<th>Comentario</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Lorem Ipsum</td>
											<td>Andrés Castillo</td>
											<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
										</tr>
										<tr>
											<td>Lorem Ipsum</td>
											<td>Andrés Castillo</td>
											<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
										</tr>
										<tr>
											<td>Lorem Ipsum</td>
											<td>Andrés Castillo</td>
											<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
										</tr>
										<tr>
											<td>Lorem Ipsum</td>
											<td>Andrés Castillo</td>
											<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mt-4 mt-lg-0">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Últimos usuarios</h5>
							
							<div class="table-responsive">
								<table class="table table-bordered table-condensed">
									<thead class="text-center">
										<tr>
											<th>Usuario</th>
											<th>Fecha registro</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Andrés Castillo</td>
											<td>17 / 02 / 2021</td>
										</tr>
										<tr>
											<td>Andrés Castillo</td>
											<td>17 / 02 / 2021</td>
										</tr>
										<tr>
											<td>Andrés Castillo</td>
											<td>17 / 02 / 2021</td>
										</tr>
										<tr>
											<td>Andrés Castillo</td>
											<td>17 / 02 / 2021</td>
										</tr>
										<tr>
											<td>Andrés Castillo</td>
											<td>17 / 02 / 2021</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>