import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { MenuComponent } from './componentscommon/menu/menu.component';


@NgModule({
  declarations: [CoreComponent, MenuComponent],
  imports: [
    CommonModule,
    CoreRoutingModule
  ],
  exports:[MenuComponent]
})
export class CoreModule { }
