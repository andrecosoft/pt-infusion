import { Injectable } from '@angular/core';
import { HttpClient, HttpParams , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Rest {

    private enpoint:string = '';
    private url_endpoint_ws:string = '';
    private params:any = {};

    constructor(private httpClient: HttpClient) {
        this.url_endpoint_ws = environment.END_POINT_URL;
    }

    setParams(params:any){
        this.params = params;
    }

    setEndPoint(enpoint:any){
        this.enpoint = enpoint;
    }

    get(){
        let headers = null;
        let params = '';

        if(this.params){
            params = '/' + this.params;
        }

        headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'cache-control': 'no-cache'
        });
        
        return this.httpClient.get(
            this.url_endpoint_ws + this.enpoint + params,
            { headers: headers }
        );
    }

    post(){
       let headers = null;
        
        headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'cache-control': 'no-cache'
        });

        return this.httpClient.post(
                this.url_endpoint_ws + this.enpoint,
                this.params,
                { headers: headers }
        );
    }

    put(entity_id:number){
        let headers = null;
        let params = '';

        if(this.params){
            params = '/' + this.params;
        }         
         headers = new HttpHeaders({
             'Content-Type': 'application/json',
             'Accept': '*/*',
             'cache-control': 'no-cache'
         });
 
         return this.httpClient.put(
                 this.url_endpoint_ws + this.enpoint + '/' + entity_id,
                 this.params,
                 { headers: headers }
         );
     }

    delete(entity_id:number){
        let headers = null;
         
         headers = new HttpHeaders({
             'Content-Type': 'application/json',
             'Accept': '*/*',
             'cache-control': 'no-cache'
         });
 
         return this.httpClient.delete(
                 this.url_endpoint_ws + this.enpoint + '/' + entity_id,
                 { headers: headers }
         );
     }

}