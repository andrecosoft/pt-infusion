import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { EnterService } from './shared/services/enter.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email:string = 'andres@andres.com';
  public password:string = '123456';

  constructor(private enter_service:EnterService,private router: Router,private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
  }

  /*
    Por aspectos de tiempo se da ingreso al sistema con 
    on click.
  */

  loginSystem(){
    this.spinner.show();
    this.enter_service.validateLogin(this.email,this.password)
    .then((response:any) =>{
      this.spinner.hide();
      if(response.success){
        this.router.navigate(['dashboard/principal']);
      }
    }).catch((e)=>{
    })
  }

}
