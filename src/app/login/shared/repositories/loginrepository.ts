import { Injectable } from '@angular/core';
import { Rest } from 'src/app/core/librerias/Rest';

@Injectable({
  providedIn: 'root'
})
export class Loginrepository {

  constructor(private rest:Rest) { }

  validateAcces(email:string,password:string){
    this.rest.setParams({
      email:email,
      password:password
    });
    this.rest.setEndPoint('api/login/ingreso-usuario');
    return this.rest.post().toPromise();
  }
}
