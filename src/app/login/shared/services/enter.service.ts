import { Injectable } from '@angular/core';
import { Loginrepository } from '../repositories/loginrepository';

@Injectable({
  providedIn: 'root'
})
export class EnterService {

  constructor(private login_repository:Loginrepository) { 

  }


  validateLogin(email:string,password:string){

    
    // ---------------------------
    // Aqui se puede hacer todo tipo de logica antes de enviar al
    // web service, incuso se puede llamar a otros services.
    // ---------------------------




    return  this.login_repository.validateAcces(email,password);
  }

}
