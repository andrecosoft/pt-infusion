import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { PrincipalComponent } from './pages/principal/principal.component';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [DashboardComponent, PrincipalComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CoreModule
  ]
})
export class DashboardModule { }
