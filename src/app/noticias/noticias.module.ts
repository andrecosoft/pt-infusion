import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoticiasRoutingModule } from './noticias-routing.module';
import { NoticiasComponent } from './noticias.component';
import { ListadoComponent } from './pages/listado/listado.component';
import { GestionComponent } from './pages/gestion/gestion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [NoticiasComponent, ListadoComponent, GestionComponent],
  imports: [
    CommonModule,
    NoticiasRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    NgxSpinnerModule
  ]
})
export class NoticiasModule { }
