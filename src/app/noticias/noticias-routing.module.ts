import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoticiasComponent } from './noticias.component';
import { GestionComponent } from './pages/gestion/gestion.component';
import { ListadoComponent } from './pages/listado/listado.component';

const routes: Routes = [
  { path: '', component: NoticiasComponent },
  { path: 'gestion/:gestion_id', component: GestionComponent },
  { path: 'gestion', component: GestionComponent },
  { path: 'listado', component: ListadoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoticiasRoutingModule { }