import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NoticeService } from '../../shared/services/notice.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  public list_all_notices:Array<any> = [];

  constructor(
    private notice_service:NoticeService,
    private router: Router,
    private spinner:NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.listAllNotices();
  }

  eliminar(notice_id:number){
    this.notice_service.deleteNotice(notice_id)
    .then((response:any)=>{
      this.listAllNotices();
    })
    .catch((e)=>{});  
  }

  edit(notice_id:number){
    this.router.navigate(['noticias/gestion/'+notice_id]);
  }

  listAllNotices(){
    this.spinner.show();
    this.notice_service.listNotices()
    .then((response:any)=>{
      this.spinner.hide();
      this.list_all_notices = response.data;
    })
    .catch((e)=>{});
  }
}
