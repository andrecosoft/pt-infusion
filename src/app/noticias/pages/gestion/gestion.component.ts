import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NoticeService } from '../../shared/services/notice.service';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.css']
})
export class GestionComponent implements OnInit {

  public form_notices:any;
  public mode_crud:string = 'SAVE';
  public notice_id_edit:number = 0;
  public msg_system:string = '';

  
  constructor(
    private formBuilder:FormBuilder,
    private router: Router,
    private notice_service:NoticeService,
    private activatedRoute: ActivatedRoute,
    private spinner:NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.form_notices = this.formBuilder.group({
      title: ['',Validators.required],
      content: ['',Validators.required],
      autor: ['',Validators.required]
    });

    this.notice_id_edit = this.activatedRoute.snapshot.params.gestion_id;
    if(this.notice_id_edit){
      this.mode_crud = 'UPDATE';

      this.spinner.show();
      this.notice_service.noticeInformation(this.notice_id_edit)
      .then((response:any)=>{
        this.spinner.hide();
        this.form_notices.patchValue({title:response.data.title,content:response.data.content,autor:response.data.autor});
      })
    }
  }

  saveNotice(notice_information:any){
    if(this.form_notices.valid){
        if(this.mode_crud == 'SAVE'){
          this.spinner.show();
          this.notice_service.saveNotice(notice_information.title,notice_information.content,notice_information.autor)
          .then((response:any)=>{
              this.spinner.hide();
              this.msg_system = 'Noticia guardada con exito';
              setTimeout(()=>{this.router.navigate(['noticias/listado']);},2000);
          });
        }else{
          this.spinner.show();
          this.notice_service.updateNotice(this.notice_id_edit,notice_information.title,notice_information.content,notice_information.autor)
          .then((response:any)=>{
            this.spinner.hide();
            this.msg_system = 'Noticia actualizada con exito';
            setTimeout(()=>{this.msg_system = '';},2000);
          })
        }
    }else{
        alert('Todos los campos son requeridos');
    }
  }

}
