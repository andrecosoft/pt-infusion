import { Injectable } from '@angular/core';
import { Noticerepository } from '../repositories/noticerepository';

@Injectable({
  providedIn: 'root'
})
export class NoticeService {

  constructor(private notice_repository:Noticerepository) { }

  saveNotice(title:string,content:string,autor:string){

    // ---------------------------
    // Aqui se puede hacer todo tipo de logica antes de enviar al
    // web service, incuso se puede llamar a otros services.
    // ---------------------------



    return this.notice_repository.save(title,content,autor);
  }

  updateNotice(notice_id:number,title:string,content:string,autor:string){

    // ---------------------------
    // Aqui se puede hacer todo tipo de logica antes de enviar al
    // web service, incuso se puede llamar a otros services.
    // ---------------------------



    return this.notice_repository.update(notice_id,title,content,autor);
  }

  listNotices(){

    // ---------------------------
    // Aqui se puede hacer todo tipo de logica antes de enviar al
    // web service, incuso se puede llamar a otros services.
    // ---------------------------



    return this.notice_repository.allNotices();
  }


  deleteNotice(notice_id:number){

    // ---------------------------
    // Aqui se puede hacer todo tipo de logica antes de enviar al
    // web service, incuso se puede llamar a otros services.
    // ---------------------------



    return this.notice_repository.delete(notice_id);
  } 
  
  noticeInformation(notice_id:number){

    // ---------------------------
    // Aqui se puede hacer todo tipo de logica antes de enviar al
    // web service, incuso se puede llamar a otros services.
    // ---------------------------



    return this.notice_repository.show(notice_id);
  } 

}
