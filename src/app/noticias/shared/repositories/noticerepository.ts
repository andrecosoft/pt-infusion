import { Injectable } from '@angular/core';
import { Rest } from 'src/app/core/librerias/Rest';

@Injectable({
  providedIn: 'root'
})
export class Noticerepository {

  constructor(private rest:Rest) { }

  save(title:string,content:string,autor:string){
    this.rest.setParams({
      title:title,
      content:content,
      autor:autor
    });
    this.rest.setEndPoint('api/notice/crud');
    return this.rest.post().toPromise();    
  }

  update(notice_id:number,title:string,content:string,autor:string){
    this.rest.setParams({
      title:title,
      content:content,
      autor:autor
    });
    this.rest.setEndPoint('api/notice/crud');
    return this.rest.put(notice_id).toPromise();    
  }

  

  allNotices(){
    this.rest.setParams(null);
    this.rest.setEndPoint('api/notice/all-notices');
    return this.rest.get().toPromise();    
  }

  delete(notice_id:number){
    this.rest.setParams(null);
    this.rest.setEndPoint('api/notice/crud');
    return this.rest.delete(notice_id).toPromise();    
  }

  show(notice_id:number){
    this.rest.setParams(notice_id);
    this.rest.setEndPoint('api/notice/crud');
    return this.rest.get().toPromise();    
  }

}
